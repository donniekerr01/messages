/**
 * Created by donkerr on 1/8/15.
 */
Handlebars.registerHelper("formatDate", function(timestamp) {
    return moment(timestamp).format('MM-DD-YYYY hh:mm:ss')
});
Handlebars.registerHelper("commentCount", function(parentid) {
    return Messages.find({parentId:parentid}).count()
});
Template.messages.rendered = function(){

}

Template.messages.helpers({
    messages:function () {
        return Messages.find({parentId:0},{ sort:{timestamp:-1} });
    },
    comments:function (parentid) {

        var c = Messages.find({parentId:parentid},{ sort:{timestamp:-1} })

        return c;
    }
});

Template.messages.events({
    'click .toggleComments':function(e){

        var show = $(e.currentTarget).data('show');
        var table = $(e.currentTarget).data('comments');
        if(show){
            $(table).show();
            $(e.currentTarget).data('show',false);

        }
        else {
            $(table).hide();
            $(e.currentTarget).data('show',true);

        }
    },
    'click #sendSuccess':function(e){
        var m = $('#message').val();
        var p = 0;
        var c = 'alert-success'
        sendMessage(m,p,c)
    },
    'click #sendInfo':function(e){
        var m = $('#message').val();
        var p = 0;
        var c = 'alert-info'
        sendMessage(m,p,c)

    },
    'click #sendWarning':function(e){
        var m = $('#message').val();
        var p = 0;
        var c = 'alert-warning'
        sendMessage(m,p,c)

    },
    'click #sendDanger':function(e){
        var m = $('#message').val();
        var p = 0;
        var c = 'alert-danger'
        sendMessage(m,p,c)

    },
    'click #commentSuccess':function(e){
        var m = $('#comment').val();
        var p = $(e.currentTarget).data('parentid');
        var c = 'alert-success'
        sendMessage(m,p,c)
    },
    'click #commentInfo':function(e){
        var m = $('#comment').val();
        var p = $(e.currentTarget).data('parentid');
        var c = 'alert-info'
        sendMessage(m,p,c)

    },
    'click #commentWarning':function(e){
        var m = $('#comment').val();
        var p = $(e.currentTarget).data('parentid');
        var c = 'alert-warning'
        sendMessage(m,p,c)

    },
    'click #commentDanger':function(e){
        var m = $('#comment').val();
        var p = $(e.currentTarget).data('parentid');
        var c = 'alert-danger'
        sendMessage(m,p,c)

    }
})

function sendMessage(message,parentid,color){
    var doc = {
        userId: Meteor.userId(),
        username: Meteor.user().username,
        message: message,
        timestamp: new Date(),
        class: color,
        parentId: parentid
    }
    Meteor.call('sendPush',doc.username,doc.username,doc.message,+1,{});
    Messages.insert(doc);

}