Package.describe({
  name: 'doubletake:messages',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: 'MeterX Messages Package',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.0.3.1');
    api.use(['templating','mizzao:bootstrap-3']);
    api.addFiles('messages.html','client');
    api.addFiles('messages.css','client');
    api.addFiles('messages.js','client');
    //api.addFiles('server.js','server');
    //api.addFiles('both.js',['client','server']);
});

Package.onTest(function(api) {
 // api.use('tinytest');
 // api.use('doubletake:messages');

});
